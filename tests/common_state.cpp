/**
 * @file tests/common_state.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/context/State.hpp>
#include <artis-star/common/time/DoubleTime.hpp>

#define BOOST_TEST_MODULE Common_State_Tests

#include <boost/test/unit_test.hpp>

/*************************************************
 * Tests
 *************************************************/

BOOST_AUTO_TEST_CASE(Common_State_TestCase_1)
{
  artis::common::context::State<artis::common::DoubleTime> state;

  BOOST_REQUIRE_EQUAL(state.last_time(), -1);
  BOOST_REQUIRE_EQUAL(state.next_time(), -1);
}

BOOST_AUTO_TEST_CASE(Common_State_TestCase_2)
{
  artis::common::context::State<artis::common::DoubleTime> state;
  artis::common::event::Value first_value{1};
  artis::common::event::Value second_value{2};

  state.add_state(0, first_value);
  state.add_state(1, second_value);

  BOOST_CHECK(state.get_state(0) == first_value);
  BOOST_CHECK(state.get_state(1) == second_value);

  BOOST_CHECK(state.get_state(0) == artis::common::event::Value{1});
  BOOST_CHECK(state.get_state(1) == artis::common::event::Value{2});
}