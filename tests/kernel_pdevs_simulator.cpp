/**
 * @file tests/common_model.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "artis-star/kernel/pdevs/Dynamics.hpp"
#include "artis-star/kernel/pdevs/Simulator.hpp"
#include <artis-star/common/time/DoubleTime.hpp>

#define BOOST_TEST_MODULE Kernel_Pdevs_Simulator_Tests

#include <boost/test/unit_test.hpp>

/*************************************************
 * Tests
 *************************************************/

BOOST_AUTO_TEST_CASE(Kernel_Pdevs_Simulator_TestCase_1)
{
  class Inactive : public artis::pdevs::Dynamics<artis::common::DoubleTime, Inactive> {
  public:
    Inactive(const std::string &name,
             const artis::pdevs::Context<artis::common::DoubleTime, Inactive, artis::common::NoParameters> &context)
      : artis::pdevs::Dynamics<artis::common::DoubleTime, Inactive, artis::common::NoParameters>(name, context) {}
  };

  artis::pdevs::Simulator<artis::common::DoubleTime, Inactive> model("A", {});

  BOOST_REQUIRE_EQUAL(model.get_name(), "A");
}