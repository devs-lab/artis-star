/**
 * @file tests/common_value.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "artis-star/common/event/Value.hpp"

#define BOOST_TEST_MODULE Common_Value_Tests

#include <boost/test/unit_test.hpp>

/*************************************************
 * Tests
 *************************************************/

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_1)
{
  artis::common::event::Value first_value{1};
  artis::common::event::Value second_value{1};

  BOOST_CHECK(first_value == second_value);
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_2)
{
  artis::common::event::Value int_value{1};
  artis::common::event::Value unsigned_int_value{(unsigned int) 1};
  artis::common::event::Value double_value{0.5};
  artis::common::event::Value bool_value{true};
  artis::common::event::Value char_value{'a'};

  BOOST_REQUIRE_EQUAL(int_value.to_string(), "1");
  BOOST_REQUIRE_EQUAL(unsigned_int_value.to_string(), "1");
  BOOST_REQUIRE_EQUAL(double_value.to_string(), "0.5");
  BOOST_REQUIRE_EQUAL(bool_value.to_string(), "true");
  BOOST_REQUIRE_EQUAL(char_value.to_string(), "a");
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_3)
{
  int int_values[] = {1, 2, 3};
  artis::common::event::Value int_value{int_values, 3};
  unsigned int unsigned_int_values[] = {1, 2, 3};
  artis::common::event::Value unsigned_int_value{unsigned_int_values, 3};
  double double_values[] = {0.5, 0.6, 1.2};
  artis::common::event::Value double_value{double_values, 3};
  bool bool_values[] = {true, false, true, false};
  artis::common::event::Value bool_value{bool_values, 4};
  char char_values[] = {'a', 'b', 'c'};
  artis::common::event::Value char_value{char_values};

  BOOST_REQUIRE_EQUAL(int_value.to_string(), "1 2 3 ");
  BOOST_REQUIRE_EQUAL(unsigned_int_value.to_string(), "1 2 3 ");
  BOOST_REQUIRE_EQUAL(double_value.to_string(), "0.5 0.6 1.2 ");
  BOOST_REQUIRE_EQUAL(bool_value.to_string(), "true false true false ");
  BOOST_REQUIRE_EQUAL(char_value.to_string(), "abc");
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_4)
{
  std::vector<int> int_vector{1,2,3};
  artis::common::event::Value int_values{int_vector};
  std::vector<unsigned int> unsigned_int_vector{1, 2, 3};
  artis::common::event::Value unsigned_int_values{unsigned_int_vector};
  std::vector<double> double_vector{0.5, 0.6, 1.2};
  artis::common::event::Value double_values{double_vector};
  std::vector<bool> bool_vector{true, false, true, false};
  artis::common::event::Value bool_values{bool_vector};
  std::vector<char> char_vector{'a', 'b', 'c'};
  artis::common::event::Value char_values{char_vector};

  BOOST_REQUIRE_EQUAL(int_values.to_string(), "1 2 3 ");
  BOOST_REQUIRE_EQUAL(unsigned_int_values.to_string(), "1 2 3 ");
  BOOST_REQUIRE_EQUAL(double_values.to_string(), "0.5 0.6 1.2 ");
  BOOST_REQUIRE_EQUAL(bool_values.to_string(), "true false true false ");
  BOOST_REQUIRE_EQUAL(char_values.to_string(), "a b c ");

  BOOST_REQUIRE_EQUAL(int_values.size(), 3);
  BOOST_REQUIRE_EQUAL(unsigned_int_values.size(), 3);
  BOOST_REQUIRE_EQUAL(double_values.size(), 3);
  BOOST_REQUIRE_EQUAL(bool_values.size(), 4);
  BOOST_REQUIRE_EQUAL(char_values.size(), 3);
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_5)
{
  struct S {
    std::string to_string() const { return "s"; }
    bool operator==(const S& /* other */) const { return true; }
  } s;
  artis::common::event::Value value{s};

  BOOST_REQUIRE_EQUAL(value.to_string(), "s");
  BOOST_REQUIRE_EQUAL(value.size(), 1);
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_6)
{
  struct S {
    std::string to_string() const { return "s"; }
    bool operator==(const S& /* other */) const { return true; }
  };
  std::vector<S> vs{S(), S(), S(), S()};
  artis::common::event::Value value{vs};

  BOOST_REQUIRE_EQUAL(value.to_string(), "s s s s ");
  BOOST_REQUIRE_EQUAL(value.size(), 4);
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_7)
{
  artis::common::event::Value int_value{1};
  artis::common::event::Value unsigned_int_value{(unsigned int) 1};
  artis::common::event::Value double_value{0.5};
  artis::common::event::Value bool_value{true};
  artis::common::event::Value char_value{'a'};

  int a;
  unsigned int b;
  double c;
  bool d;
  char e;

  int_value(a);
  unsigned_int_value(b);
  double_value(c);
  bool_value(d);
  char_value(e);

  BOOST_REQUIRE_EQUAL(a, 1);
  BOOST_REQUIRE_EQUAL(b, 1);
  BOOST_REQUIRE_EQUAL(c, 0.5);
  BOOST_REQUIRE_EQUAL(d, true);
  BOOST_REQUIRE_EQUAL(e, 'a');
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_8)
{
  std::vector<int> int_vector{1,2,3};
  artis::common::event::Value int_values{int_vector};
  std::vector<unsigned int> unsigned_int_vector{1, 2, 3};
  artis::common::event::Value unsigned_int_values{unsigned_int_vector};
  std::vector<double> double_vector{0.5, 0.6, 1.2};
  artis::common::event::Value double_values{double_vector};
  std::vector<bool> bool_vector{true, false, true, false};
  artis::common::event::Value bool_values{bool_vector};
  std::vector<char> char_vector{'a', 'b', 'c'};
  artis::common::event::Value char_values{char_vector};

  std::vector<int> va;
  std::vector<unsigned int> vb;
  std::vector<double> vc;
  std::vector<bool> vd;
  std::vector<char> ve;

  int_values(va);
  unsigned_int_values(vb);
  double_values(vc);
  bool_values(vd);
  char_values(ve);

  BOOST_CHECK(std::equal(va.cbegin(), va.cend(), int_vector.cbegin()));
  BOOST_CHECK(std::equal(vb.cbegin(), vb.cend(), unsigned_int_vector.cbegin()));
  BOOST_CHECK(std::equal(vc.cbegin(), vc.cend(), double_vector.cbegin()));
  BOOST_CHECK(std::equal(vd.cbegin(), vd.cend(), bool_vector.cbegin()));
  BOOST_CHECK(std::equal(ve.cbegin(), ve.cend(), char_vector.cbegin()));
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_9)
{
  struct S {
    std::string to_string() const { return "s"; }
    bool operator==(const S& /* other */) const { return true; }
  };
  S s;
  artis::common::event::Value value{s};
  S s2;

  value(s2);
  BOOST_CHECK(s == s2);
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_10)
{
  struct S {
    std::string to_string() const { return "s"; }
    bool operator==(const S& /* other */) const { return true; }
  };
  std::vector<S> vs;
  artis::common::event::Value value{vs};
  std::vector<S> vs2;

  value(vs2);
  BOOST_CHECK(std::equal(vs.cbegin(), vs.cend(), vs2.cbegin()));
}

BOOST_AUTO_TEST_CASE(Common_Value_TestCase_11)
{
  std::string s{"artis"};
  artis::common::event::Value value{s};
  std::string s2;

  value(s2);
  BOOST_REQUIRE_EQUAL(value.to_string(), "artis");
  BOOST_CHECK(s == s2);
}
