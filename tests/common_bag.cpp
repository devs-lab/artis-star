/**
 * @file tests/common_bag.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/event/Bag.hpp>
#include <artis-star/common/time/DoubleTime.hpp>

#define BOOST_TEST_MODULE Common_Bag_Tests

#include <boost/test/unit_test.hpp>

/*************************************************
 * Tests
 *************************************************/

BOOST_AUTO_TEST_CASE(Common_Bag_TestCase_1)
{
  artis::common::event::Bag<artis::common::DoubleTime> bag;

  BOOST_REQUIRE_EQUAL(bag.size(), 0);
}

BOOST_AUTO_TEST_CASE(Common_Bag_TestCase_2)
{
  artis::common::event::Bag<artis::common::DoubleTime> bag;
  artis::common::event::ExternalEvent<artis::common::DoubleTime> event(0);
  int data{};

  event.data((int)1);
  bag.push_back(event);
  bag.back().data()(data);

  BOOST_REQUIRE_EQUAL(bag.size(), 1);
  BOOST_REQUIRE_EQUAL(data, 1);
}