/**
 * @file tests/common_context.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/context/Context.hpp>
#include <artis-star/common/time/DoubleTime.hpp>

#define BOOST_TEST_MODULE Common_Context_Tests

#include <boost/test/unit_test.hpp>

/*************************************************
 * Tests
 *************************************************/

BOOST_AUTO_TEST_CASE(Common_Context_TestCase_1)
{
  artis::common::context::Context<artis::common::DoubleTime> context;

  BOOST_REQUIRE_EQUAL(context.begin(), -1);
  BOOST_REQUIRE_EQUAL(context.end(), -1);
  BOOST_REQUIRE_EQUAL(context.valid(), false);
}

BOOST_AUTO_TEST_CASE(Common_Context_TestCase_2)
{
  artis::common::context::Context<artis::common::DoubleTime> context(0, 500);

  BOOST_REQUIRE_EQUAL(context.begin(), 0);
  BOOST_REQUIRE_EQUAL(context.end(), 500);
  BOOST_REQUIRE_EQUAL(context.valid(), false);
}

BOOST_AUTO_TEST_CASE(Common_Context_TestCase_3)
{
  artis::common::context::Context<artis::common::DoubleTime> context(0, 500);

  BOOST_REQUIRE_EQUAL(context.to_string(), "begin: 0.000000; end: 500.000000; valid: false;"
                                           " state: { last_time: -1.000000; next_time: -1.000000;"
                                           " values: { states: [ ] } ; sub_states: [ ] }");
}