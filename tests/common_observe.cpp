/**
 * @file tests/common_observe.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/context/Context.hpp>
#include <artis-star/common/observer/Observer.hpp>
#include <artis-star/common/observer/View.hpp>
#include <artis-star/common/time/DoubleTime.hpp>

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#define BOOST_TEST_MODULE Common_Observe_Tests

#include <boost/test/unit_test.hpp>

/*************************************************
 * Tests
 *************************************************/

template<typename M>
class OnlyOneGraphManager :
  public artis::pdevs::GraphManager<artis::common::DoubleTime> {
public:
  enum sub_models {
    OneM
  };

  OnlyOneGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                      const artis::common::NoParameters &parameters,
                      const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<artis::common::DoubleTime>(coordinator, parameters, graph_parameters),
    model("a", parameters) {
    this->add_child(OneM, &model);
  }

  ~OnlyOneGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime, M> model;
};

class MyDynamics : public artis::pdevs::Dynamics<artis::common::DoubleTime, MyDynamics> {
public:
  struct vars {
    enum values {
      VALUE = 0
    };
  };

  MyDynamics(const std::string &name,
             const artis::pdevs::Context<artis::common::DoubleTime, MyDynamics, artis::common::NoParameters> &context)
    : artis::pdevs::Dynamics<artis::common::DoubleTime, MyDynamics, artis::common::NoParameters>(name, context),
      _counter(1) {
    observables({{vars::VALUE, "value"}});
  }

  void dint(const artis::common::DoubleTime::type & /* t */) override {
    if (_counter < 100) { ++_counter; } else { _counter = 1; }
  }

  void start(const artis::common::DoubleTime::type & /* t */) override {
    _counter = 1;
  }

  artis::common::DoubleTime::type ta(const artis::common::DoubleTime::type & /* t */) const override {
    return 1;
  }

  artis::common::event::Value observe(const artis::common::DoubleTime::type & /* t */,
                                      unsigned int index) const override {
    if (index == vars::VALUE) {
      return (unsigned int) _counter;
    }
    return {};
  }

private:
  unsigned int _counter;
};

BOOST_AUTO_TEST_CASE(Common_Observe_TestCase_1)
{
  artis::pdevs::Simulator<artis::common::DoubleTime, MyDynamics> model("A", {});
  artis::common::observer::View<artis::common::DoubleTime> view;
  artis::common::event::Value ref{(unsigned int) 1};

  view.attachModel(&model);
  view.selector("value", {MyDynamics::vars::VALUE});
  view.observe(0, true);

  BOOST_REQUIRE_EQUAL(view.get("value").back().second, ref);
}

BOOST_AUTO_TEST_CASE(Common_Observe_TestCase_2)
{
  artis::pdevs::Simulator<artis::common::DoubleTime, MyDynamics> model("A", {});
  auto view = new artis::common::observer::View<artis::common::DoubleTime>();
  artis::common::observer::Observer<artis::common::DoubleTime> observer(&model);
  artis::common::event::Value ref{(unsigned int) 1};

  view->selector("value", {MyDynamics::vars::VALUE});
  observer.attachView("global", view);
  observer.init();
  observer.observe(0, 1, true);

  BOOST_REQUIRE_EQUAL(observer.views().at("global")->get("value").back().second, ref);
  BOOST_REQUIRE_EQUAL(observer.view("global").get("value").back().second, ref);
}

BOOST_AUTO_TEST_CASE(Common_Observe_TestCase_3)
{
  artis::pdevs::Simulator<artis::common::DoubleTime, MyDynamics> model("A", {});
  auto view = new artis::common::observer::View<artis::common::DoubleTime>();
  artis::common::observer::Observer<artis::common::DoubleTime> observer(&model);
  artis::common::event::Value ref{(unsigned int) 1};

  view->selector("value", {OnlyOneGraphManager<MyDynamics>::OneM, MyDynamics::vars::VALUE});

  artis::common::context::Context<artis::common::DoubleTime> context(0, 200);
  artis::common::RootCoordinator<artis::common::DoubleTime, artis::pdevs::Coordinator<
    artis::common::DoubleTime, OnlyOneGraphManager<MyDynamics>,
    artis::common::NoParameters, artis::common::NoParameters>> rc(context, "root", artis::common::NoParameters(),
                                                                  artis::common::NoParameters());

  rc.attachView("global", view);
  rc.run(context);

  BOOST_REQUIRE_EQUAL(rc.observer().view("global").get("value").back().second, ref);
}

BOOST_AUTO_TEST_CASE(Common_Observe_TestCase_4)
{
  artis::pdevs::Simulator<artis::common::DoubleTime, MyDynamics> model("A", {});
  auto view = new artis::common::observer::View<artis::common::DoubleTime>();
  artis::common::observer::Observer<artis::common::DoubleTime> observer(&model);
  artis::common::event::Value ref1{(unsigned int) 11};
  artis::common::event::Value ref2{(unsigned int) 21};

  view->selector("value", {OnlyOneGraphManager<MyDynamics>::OneM, MyDynamics::vars::VALUE});

  artis::common::context::Context<artis::common::DoubleTime> context(0, 200);
  artis::common::RootCoordinator<artis::common::DoubleTime, artis::pdevs::Coordinator<
    artis::common::DoubleTime, OnlyOneGraphManager<MyDynamics>,
    artis::common::NoParameters, artis::common::NoParameters>> rc(context, "root", artis::common::NoParameters(),
                                                                  artis::common::NoParameters());

  rc.attachView("global", view);
  rc.switch_to_timed_observer(0.1);
  rc.start(context);
  rc.run(10);

  BOOST_REQUIRE_EQUAL(rc.observer().view("global").get("value").back().second, ref1);
  BOOST_REQUIRE_EQUAL(rc.observer().view("global").get("value").size(), 100);

  rc.run(10);

  BOOST_REQUIRE_EQUAL(rc.observer().view("global").get("value").back().second, ref2);
  BOOST_REQUIRE_EQUAL(rc.observer().view("global").get("value").size(), 100);
}