/**
 * @file common/utils/Trace.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_UTILS_TRACE
#define COMMON_UTILS_TRACE

#include <artis-star/common/utils/FormalismType.hpp>
#include <artis-star/common/utils/FunctionType.hpp>
#include <artis-star/common/utils/LevelType.hpp>

#include <algorithm>
#include <iterator>
#include <memory>
#include <mutex>
#include <sstream>
#include <vector>

namespace artis::common {

template<typename Time>
class TraceElement {
public:
  TraceElement()
    : _time(Time::null), _formalism_type(FormalismType::NONE),
      _function_type(FunctionType::NONE), _level_type(LevelType::NONE) {}

  TraceElement(const std::string &model_name, typename Time::type time,
               const FormalismType::Values &formalism_type,
               const FunctionType::Values &function_type, const LevelType::Values &level_type)
    : _model_name(model_name), _time(time), _formalism_type(formalism_type),
      _function_type(function_type), _level_type(level_type) {}

  virtual ~TraceElement() {}

  const std::string &get_comment() const { return _comment; }

  const std::string &get_model_name() const { return _model_name; }

  typename Time::type get_time() const { return _time; }

  const FormalismType::Values &get_formalism_type() const { return _formalism_type; }

  const FunctionType::Values &get_function_type() const { return _function_type; }

  const LevelType::Values &get_level_type() const { return _level_type; }

  void set_comment(const std::string &comment) { _comment = comment; }

  std::string to_string() const {
    std::ostringstream ss;

    ss << "TRACE: " << get_model_name() << " at " << get_time() << " <";
    ss << FormalismType::to_string(get_formalism_type()) << ", ";
    ss << FunctionType::to_string(get_function_type()) << ", ";
    ss << LevelType::to_string(get_level_type()) << ">";
    if (not get_comment().empty()) {
      ss << " => " << get_comment();
    }
    return ss.str();
  }

private:
  std::string _model_name;
  typename Time::type _time;
  FormalismType::Values _formalism_type;
  FunctionType::Values _function_type;
  LevelType::Values _level_type;
  std::string _comment;
};

template<typename Time>
class TraceElements : public std::vector<TraceElement<Time> > {
public:
  TraceElements() {}

  virtual ~TraceElements() {}

  TraceElements filter_model_name(const std::string &model_name) const {
    TraceElements<Time> result;

    std::copy_if(TraceElements<Time>::begin(),
                 TraceElements<Time>::end(), std::back_inserter(result),
                 [model_name](TraceElement<Time> const &x) {
                   return x.get_model_name() == model_name;
                 });
    return result;
  }

  TraceElements filter_time(typename Time::type time) const {
    TraceElements result;

    std::copy_if(TraceElements<Time>::begin(),
                 TraceElements<Time>::end(), std::back_inserter(result),
                 [time](TraceElement<Time> const &x) { return x.get_time() == time; });
    return result;
  }

  TraceElements filter_formalism_type(const FormalismType::Values &type) const {
    TraceElements result;

    std::copy_if(TraceElements<Time>::begin(),
                 TraceElements<Time>::end(), std::back_inserter(result),
                 [type](TraceElement<Time> const &x) { return x.get_formalism_type() == type; });
    return result;
  }

  TraceElements filter_function_type(const FunctionType::Values &type) const {
    TraceElements result;

    std::copy_if(TraceElements<Time>::begin(),
                 TraceElements<Time>::end(), std::back_inserter(result),
                 [type](TraceElement<Time> const &x) { return x.get_function_type() == type; });
    return result;
  }

  TraceElements filter_level_type(const LevelType::Values &type) const {
    TraceElements result;

    std::copy_if(TraceElements<Time>::begin(),
                 TraceElements<Time>::end(), std::back_inserter(result),
                 [type](TraceElement<Time> const &x) { return x.get_level_type() == type; });
    return result;
  }

  std::string to_string() const {
    std::ostringstream ss;

    for (typename TraceElements<Time>::const_iterator it = TraceElements<Time>::begin();
         it != TraceElements<Time>::end(); ++it) {
      ss << "TRACE: " << it->get_model_name() << " at " << it->get_time() << " <";
      ss << FormalismType::to_string(it->get_formalism_type()) << ", ";
      ss << FunctionType::to_string(it->get_function_type()) << ", ";
      ss << LevelType::to_string(it->get_level_type()) << ">";
      if (not it->get_comment().empty()) {
        ss << " => " << it->get_comment();
      }
      ss << std::endl;
    }
    return ss.str();
  }
};

template<typename Time>
class Trace {
public:
  virtual ~Trace() {}

  static Trace &trace() {
//                std::call_once(_flag, []() { _instance.reset(new Trace()); });
    if (!_instance) {
      _instance.reset(new Trace());
    }
    return *_instance;
  }

  void clear() { _trace.clear(); }

  const TraceElements<Time> &elements() const { return _trace; }

  void flush() {
    std::lock_guard <std::mutex> lock(_mutex);

    if (_sstream) {
      _element.set_comment(_sstream->str());
      delete _sstream;
      _sstream = 0;
    }
    _trace.push_back(_element);
  }

  std::mutex &mutex() { return _mutex; }

  void set_element(const TraceElement<Time> &element) { _element = element; }

  std::ostringstream &sstream() {
    if (_sstream == 0) {
      _sstream = new std::ostringstream();
    }
    return *_sstream;
  }

private:
  Trace() { _sstream = 0; }

  static std::shared_ptr <Trace<Time>> _instance;
//            static std::once_flag _flag;

  TraceElements<Time> _trace;
  TraceElement<Time> _element;
  std::ostringstream *_sstream;
  std::mutex _mutex;
};

} // namespace artis common

template<typename Time>
artis::common::Trace<Time> &
operator<<(artis::common::Trace<Time> &trace, const artis::common::TraceElement<Time> &e) {
  std::lock_guard <std::mutex> lock(trace.mutex());

  trace.set_element(e);
  return trace;
}

template<typename Time>
artis::common::Trace<Time> &operator<<(artis::common::Trace<Time> &trace, const std::string &str) {
  std::lock_guard <std::mutex> lock(trace.mutex());

  trace.sstream() << str;
  return trace;
}

template<typename Time>
artis::common::Trace<Time> &operator<<(artis::common::Trace<Time> &trace, typename Time::type t) {
  std::lock_guard <std::mutex> lock(trace.mutex());

  trace.sstream() << t;
  return trace;
}

template<typename Time>
std::shared_ptr <artis::common::Trace<Time>> artis::common::Trace<Time>::_instance = nullptr;

//template<typename Time>
//std::once_flag artis::common::Trace<Time>::_flag;

#endif
