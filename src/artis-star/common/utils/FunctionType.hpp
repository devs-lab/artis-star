/**
 * @file common/utils/TraceType.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_UTILS_FUNCTION_TYPE
#define COMMON_UTILS_FUNCTION_TYPE

#include <string>

namespace artis::common {

class FunctionType {
public:
  enum Values {
    NONE = 0, CONSTRUCTOR, FINISH, I_MESSAGE, POST_EVENT, S_MESSAGE, Y_MESSAGE,
    DELTA_INT, DELTA_EXT, DELTA_CONF, TA, LAMBDA, START, OUTPUT, TRANSITION
  };

  static std::string to_string(const FunctionType::Values &s) {
    switch (s) {
      case NONE:
        return "none";
      case CONSTRUCTOR:
        return "constructor";
      case FINISH:
        return "finish";
      case I_MESSAGE:
        return "i_message";
      case POST_EVENT:
        return "post_event";
      case S_MESSAGE:
        return "s_message";
      case Y_MESSAGE:
        return "y_message";
      case DELTA_INT:
        return "delta_int";
      case DELTA_EXT:
        return "delta_ext";
      case DELTA_CONF:
        return "delta_conf";
      case TA:
        return "ta";
      case LAMBDA:
        return "lambda";
      case START:
        return "start";
      case OUTPUT:
        return "output";
      case TRANSITION:
        return "transition";
    };
    return "";
  }
};

}

#endif
