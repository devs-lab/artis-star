/**
 * @file common/States.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_STATE_STATES_HPP
#define COMMON_STATE_STATES_HPP

#include "artis-star/common/context/State.hpp"
#include "artis-star/common/state/Any.hpp"
#include "artis-star/common/utils/Macro.hpp"

#include <vector>

namespace artis::common::state {

template<typename Time, typename Dyn>
class States {
public:
  template<typename W>
  struct element {
    unsigned int index;
    const std::string name;
    W Dyn::* var;

    element(unsigned int index, const std::string &name, W Dyn::* var)
      : index(index), name(name), var(var) {}
  };

  States() {}

  virtual ~States() {}

  common::state::Any &get(unsigned int index) { return states.at(index); }

  const common::state::Any &get(unsigned int index) const { return states.at(index); }

  unsigned int state_number() const { return states.size(); }

  template<typename W>
  void S_(std::initializer_list<element<W>> list) {
    for (typename std::initializer_list<element<W> >::iterator it =
      list.begin(); it != list.end(); ++it) {
      if (states.size() <= it->index) {
        states.resize(it->index + 1, common::state::Any());
        state_names.resize(it->index + 1, std::string());
      }
      states[it->index] = it->var;
      state_names[it->index] = it->name;
    }
  }

  void restore(Dyn *model, const common::context::State<Time> &state) {
    unsigned int index = 0;

    for (typename std::vector<common::state::Any>::iterator it = states.begin();
         it != states.end(); ++it) {
      if (not it->is_null()) {
        it->restore<Dyn>(model, state.get_state(index));
      }
      ++index;
    }
  }

  void save(const Dyn *model, common::context::State<Time> &state) const {
    unsigned int index = 0;

    for (typename std::vector<common::state::Any>::const_iterator it =
      states.begin(); it != states.end(); ++it) {
      if (not it->is_null()) {
        state.add_state(index, it->save<Dyn>(model));
      }
      ++index;
    }
  }

  template<typename W>
  void state_(unsigned int index, const std::string &name, W Dyn::* var) {
    if (states.size() <= index) {
      states.resize(index + 1, common::state::Any());
      state_names.resize(index + 1, std::string());
    }
    states[index] = common::state::Any(var);
    state_names[index] = name;
  }

private:
  std::vector<common::state::Any> states;
  std::vector<std::string> state_names;
};

}

#define DECLARE_STATE(W, index, var)                                    \
        this->state_ < W >(index, std::string(ESCAPEQUOTE(index)), var)

#define DECLARE_STATES(W, L) this-> template S_< W >(UNWRAP2 L)

#endif
