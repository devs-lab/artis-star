/**
 * @file common/context/StateValues.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_CONTEXT_STATE_VALUES_HPP
#define COMMON_CONTEXT_STATE_VALUES_HPP

#include "artis-star/common/event/Value.hpp"

#include <map>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>

namespace artis::common::context {

class StateValues {
public:
  StateValues() = default;
  StateValues(const StateValues&) = default;
  StateValues(StateValues&&) = default;

  virtual ~StateValues() = default;

  void add_state(unsigned int key, const event::Value &value) { _states[key] = value; }

  const event::Value &get_state(unsigned int key) const {
    Values::const_iterator it = _states.find(key);

    if (it != _states.end()) {
      return it->second;
    }

    assert(false);

    return it->second;
  }

  StateValues& operator=(const StateValues& values) {
    _states = values._states;
    return *this;
  }

  StateValues& operator=(StateValues&& values) {
    _states = values._states;
    return *this;
  }

  std::string to_string() const {
    std::string str = "states: [ ";
    for (Values::const_iterator it = _states.begin(); it != _states.end(); ++it) {
      str += it->second.to_string() + " ";
    }
    str += "]";
    return str;
  }

private:
  typedef std::map<unsigned int, event::Value> Values;

  friend class boost::serialization::access;

  template<typename Archive>
  void serialize(Archive &ar, const unsigned int version) {
    (void) version;

    ar & _states;
  }

  Values _states;
};

}

#endif
