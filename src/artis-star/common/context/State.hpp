/**
 * @file common/context/State.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_CONTEXT_STATE_HPP
#define COMMON_CONTEXT_STATE_HPP

#include <artis-star/common/context/StateValues.hpp>

#include <map>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>

namespace artis::common::context {

template<typename Time>
class State {
  typedef std::map<unsigned int, State<Time> > Substates;

public:
  State()
    : _last_time(-1), _next_time(-1) {}

  State(const State &) = default;

  State(State &&) = default;

  virtual ~State() = default;

  void add_state(unsigned int variable_key, const event::Value &value) {
    _values.add_state(variable_key, value);
  }

  void add_sub_state(unsigned int model_key, State &state) {
    _sub_states.insert(std::make_pair(model_key, state));
  }

  const event::Value &get_state(unsigned int variable_key) const {
    return _values.get_state(variable_key);
  }

  const State<Time> &get_sub_state(unsigned int model_key) const {
    return _sub_states.find(model_key)->second;
  }

  typename Time::type last_time() const { return _last_time; }

  void last_time(typename Time::type time) { _last_time = time; }

  typename Time::type next_time() const { return _next_time; }

  void next_time(typename Time::type time) { _next_time = time; }

  State& operator=(const State& state) {
    _values = state._values;
    _sub_states = state._sub_states;
    _last_time = state._last_time;
    _next_time = state._next_time;
    return *this;
  }

  State& operator=(State&& state) {
    _values = state._values;
    _sub_states = state._sub_states;
    _last_time = state._last_time;
    _next_time = state._next_time;
    return *this;
  }

  std::string to_string() const {
    std::string str = "last_time: " + std::to_string(_last_time) +
                      "; next_time: " + std::to_string(_next_time) +
                      "; values: { " + _values.to_string() + " } ; sub_states: [ ";

    for (typename Substates::const_iterator it = _sub_states.begin();
         it != _sub_states.end(); ++it) {
      str += "{ " + it->second.to_string() + "} ";
    }
    str += "]";
    return str;
  }

private:
  friend class boost::serialization::access;

  template<typename Archive>
  void serialize(Archive &ar, const unsigned int version) {
    (void) version;

    ar & _values;
    ar & _sub_states;
    ar & _last_time;
    ar & _next_time;
  }

  StateValues _values;
  Substates _sub_states;
  typename Time::type _last_time;
  typename Time::type _next_time;
};

}

#endif
