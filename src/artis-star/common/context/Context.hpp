/**
 * @file common/context/Context.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_CONTEXT_CONTEXT_HPP
#define COMMON_CONTEXT_CONTEXT_HPP

#include <artis-star/common/context/State.hpp>

#include <boost/serialization/serialization.hpp>

namespace artis::common::context {

template<typename Time>
class Context {
public:
  Context()
    : _begin(-1), _end(-1), _valid(false) {}

  Context(const typename Time::type &begin, const typename Time::type &end)
    : _begin(begin), _end(end), _valid(false) {}

  virtual ~Context() = default;

  const typename Time::type &begin() const { return _begin; }

  const typename Time::type &end() const { return _end; }

  void end(const typename Time::type &end) { _end = end; }

  Context &operator=(const Context &context) {
    _begin = context._begin;
    _end = context._end;
    _valid = context._valid;
    _state = context._state;
    return *this;
  }

  void saved() { _valid = true; }

  const State <Time> &state() const { return _state; }

  State <Time> &state() { return _state; }

  std::string to_string() const {
    return "begin: " + std::to_string(_begin) +
           "; end: " + std::to_string(_end) +
           "; valid: " + (_valid ? "true" : "false") +
           "; state: { " + _state.to_string() + " }";
  }

  bool valid() const { return _valid; }

private:
  friend class boost::serialization::access;

  template<typename Archive>
  void serialize(Archive &ar, const unsigned int version) {
    (void) version;

    ar & _begin;
    ar & _end;
    ar & _state;
    ar & _valid;
  }

  typename Time::type _begin;
  typename Time::type _end;
  State <Time> _state;
  bool _valid;
};

}

#endif
