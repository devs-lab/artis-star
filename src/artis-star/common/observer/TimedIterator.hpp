/**
 * @file common/observer/TimedIterator.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_COMMON_OBSERVER_TIMED_ITERATOR_HPP
#define ARTIS_COMMON_OBSERVER_TIMED_ITERATOR_HPP

#include <artis-star/common/observer/DiscreteTimeIterator.hpp>

namespace artis::common::observer {

template<typename Time>
struct TimedIterator {
  typedef artis::common::observer::DiscreteTimeIterator<Time> iterator_type;
  double _begin;
  double _step;

  double begin(const std::vector <DiscreteTimeIterator<Time>> & /* its */) const { return _begin; }

  iterator_type build(const typename View<Time>::Values &view) const { return iterator_type(view, _begin, _step); }

  bool is_valid(double /* time */, const iterator_type & /* it */) const { return true; }

  double next_time(double time, const std::vector <DiscreteTimeIterator<Time>> & /* its */) const {
    return time + _step;
  }
};

}

#endif
