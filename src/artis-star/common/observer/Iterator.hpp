/**
 * @file common/observer/Iterator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_COMMON_OBSERVER_ITERATOR_HPP
#define ARTIS_COMMON_OBSERVER_ITERATOR_HPP

#include <artis-star/common/observer/View.hpp>

namespace artis::common::observer {

template<typename Time>
class Iterator {
public:
  Iterator(const typename View<Time>::Values &view)
    : _view(view), _iterator(view.begin()) {}

  virtual ~Iterator() = default;

  bool has_next() const { return _iterator != _view.end(); }

  virtual void operator++() { _iterator++; }

  virtual const std::pair<double, common::event::Value> &operator*() const { return *_iterator; }

private:
  const typename View<Time>::Values &_view;
  typename View<Time>::Values::const_iterator _iterator;
};

}

#endif