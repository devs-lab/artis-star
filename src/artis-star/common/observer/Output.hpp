/**
 * @file common/observer/Output.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_COMMON_OBSERVER_OUTPUT_HPP
#define ARTIS_COMMON_OBSERVER_OUTPUT_HPP

#include <artis-star/common/observer/Observer.hpp>
#include <artis-star/common/observer/View.hpp>

#include <boost/format.hpp>
#include <fstream>

namespace artis::common::observer {

template<typename Time, typename Iterator>
class Output {
public:
  Output(const Observer <Time> &observer)
    : _observer(observer) {}

  virtual ~Output() {}

  void operator()(double /* begin */, double end, const Iterator &iterator) const {
    const typename Observer<Time>::Views &views = _observer.views();

    for (typename Observer<Time>::Views::const_iterator it =
      views.begin(); it != views.end(); ++it) {
      std::ofstream o((boost::format("%1%.csv") % it->first).str());
      const typename View<Time>::SelectorValues &values = it->second->values();
      std::vector<typename Iterator::iterator_type> its;

      o.precision(10);
      // write header
      o << "time";
      for (typename View<Time>::SelectorValues::const_iterator itv = values.begin();
           itv != values.end(); ++itv) {
        const typename View<Time>::VariableValues &vv = itv->second;

        for (typename View<Time>::VariableValues::const_iterator itvv = vv.begin();
             itvv != vv.end(); ++itvv) {
          o << ";" << itvv->first;
          its.push_back(iterator.build(itvv->second));
        }
      }
      o << std::endl;

      // write values
      bool more = true;
      double t = iterator.begin(its);

      while (more and t <= end) {
        more = false;
        o << t;
        for (auto &dtt: its) {
          if (dtt.has_next() and iterator.is_valid(t, dtt)) {
            o << ";" << (*dtt).second.to_string();
            ++dtt;
            more = true;
          } else {
            o << ";";
          }
        }
        o << std::endl;
        t = iterator.next_time(t, its);
      }
    }
  }

private:
  const Observer <Time> &_observer;
};

}

#endif
