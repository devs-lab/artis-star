/**
 * @file common/observer/View.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_COMMON_OBSERVER_VIEW_HPP
#define ARTIS_COMMON_OBSERVER_VIEW_HPP

#include <artis-star/common/Model.hpp>
#include <artis-star/common/time/DoubleTime.hpp>
#include "artis-star/common/event/Value.hpp"

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

namespace artis::common::observer {

template<typename Time>
class View {
  typedef std::vector<int> Selector;

public:
  typedef std::vector<std::pair<double, common::event::Value>> Values;
  typedef std::map<std::string, Values> VariableValues;
  typedef std::map<std::string, VariableValues> SelectorValues;
  template<typename W>
  using ValueVector = std::vector<std::pair<double, W>>;

  enum vars {
    ALL = -1
  };

  View()
    : _model(0) {}

  virtual ~View() {}

  void attachModel(const artis::common::Model<Time> *model) { _model = model; }

  void clear() {
    for (auto &a: _values) {
      for (auto &b: a.second) {
        b.second.clear();
      }
    }
  }

  View *clone() const {
    View *v = new View();

    // v->_selectors = _selectors;
    // for (Values::const_iterator it = _values.begin(); it!= _values.end();
    //      ++it) {

    //     v->_values[it->first] = Value();
    //     Value::const_iterator itp = it->second.begin();

    //     while (itp != it->second.end()) {
    //         v->_values[it->first].push_back(*itp);
    //         ++itp;
    //     }
    // }
    // v->_model = 0;
    return v;
  }

  const Values &get(const std::string &selector_name, const std::string &variable_name) const {
    SelectorValues::const_iterator it = _values.find(selector_name);

    if (it != _values.end()) {
      VariableValues::const_iterator itv = it->second.find(variable_name);

      if (itv != it->second.end()) {
        return itv->second;
      } else {
        assert(false);
      }
    } else {
      throw std::range_error("selector name not found");
    }
  }

  template<typename W>
  ValueVector<W> get(const std::string &selector_name, const std::string &variable_name) const {
    SelectorValues::const_iterator it = _values.find(selector_name);

    if (it != _values.end()) {
      VariableValues::const_iterator itv = it->second.find(variable_name);

      if (itv != it->second.end()) {
        const Values &values = itv->second;
        ValueVector<W> result;

        for (const auto &value: values) {
          W data;

          value.second(data);
          result.push_back(std::make_pair(value.first, data));
        }
        return result;
      }
      assert(false);
    } else {
      throw std::range_error("selector name not found");
    }
  }

  const Values &get(const std::string &selector_name) const {
    SelectorValues::const_iterator it = _values.find(selector_name);

    if (it != _values.end()) {
      assert(it->second.size() == 1);

      return it->second.begin()->second;
    }
    throw std::range_error("selector name not found");
  }

  template<typename W>
  ValueVector<W> get(const std::string &selector_name) const {
    SelectorValues::const_iterator it = _values.find(selector_name);

    if (it != _values.end()) {
      assert(it->second.size() == 1);

      const Values &values = it->second.begin()->second;
      ValueVector<W> result;

      for (const auto &value: values) {
        W data;

        value.second(data);
        result.push_back(std::make_pair(value.first, data));
      }
      return result;
    }
    throw std::range_error("selector name not found");
  }

  void observe(double time, const common::Model<common::DoubleTime> *model,
               const std::string &selector_name, unsigned int variable_index) {
    std::string path = (boost::format("%1%:%2%") % model->path() %
                        model->observable_name(variable_index)).str();
    VariableValues &values = _values[selector_name];

    if (values.find(path) == values.end()) {
      values[path] = Values();
    }
    values[path].push_back(
      std::make_pair(time, model->observe(time, variable_index)));
  }

  void observe(const Selector &chain, unsigned int i,
               double time, const common::Model<common::DoubleTime> *model,
               const std::string &selector_name, unsigned int variable_index) {
    assert(model != nullptr);

    while (i < chain.size() - 1 and chain[i + 1] != ALL and model) {

      assert(chain[i] >= 0);

      model = model->get_sub_model((unsigned int) chain[i]);
      ++i;
    }
    if (i < chain.size() - 1 and chain[i + 1] == ALL) {

      assert(model != nullptr);

      for (size_t model_index = 0;
           model_index < model->get_sub_model_number(chain[i]);
           ++model_index) {

        assert(chain[i] >= 0);

        observe(chain, i + 2, time,
                model->get_sub_model((unsigned int) chain[i], model_index),
                selector_name, variable_index);
      }
    } else {
      if (model != nullptr) {
        observe(time, model, selector_name, variable_index);
      }
    }
  }

  virtual void observe(double time, bool trim) {
    for (typename Selectors::const_iterator it = _selectors.begin();
         it != _selectors.end(); ++it) {
      const common::Model<common::DoubleTime> *model = _model;

      if (it->second.size() > 1) {
        size_t i = 0;

        observe(it->second, i, time, model, it->first, it->second.back());
      } else {
        if (model != nullptr) {
          observe(time, model, it->first, it->second.back());
        }
      }
    }

    if (trim) {
      bool ok = true;
      auto it = _values.begin();

      while (it != _values.end() and ok) {
        const auto &value = it->second.begin()->second;

        if (value.back().second.is_null()) {
          ++it;
        } else {
          if (value.size() > 1) {
            if (value.back().second == value[value.size() - 2].second) {
              ++it;
            } else {
              ok = false;
            }
          } else {
            ok = false;
          }
        }
      }
      if (ok) {
        for (auto &value: _values) {
          value.second.begin()->second.pop_back();
        }
      }
    }
  }

  void selector(const std::string &name, const Selector &chain) {
    _selectors[name] = chain;
  }

  const SelectorValues &values() const { return _values; }

private:
  typedef std::map<std::string, Selector> Selectors;

  Selectors _selectors;
  SelectorValues _values;
  const artis::common::Model<Time> *_model;
};

}

#endif
