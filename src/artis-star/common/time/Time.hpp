/**
 * @file common/time/Time.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_TIME_TIME
#define COMMON_TIME_TIME

namespace artis::common {

template<typename Type, typename Limits>
struct Time {
  static Type negative_infinity;
  static Type infinity;
  static Type null;
  typedef Type type;
};

template<typename Type, typename Limits>
Type Time<Type, Limits>::negative_infinity = Limits::negative_infinity;

template<typename Type, typename Limits>
Type Time<Type, Limits>::infinity = Limits::positive_infinity;

template<typename Type, typename Limits>
Type Time<Type, Limits>::null = Limits::null;

} // namespace artis common

#endif
