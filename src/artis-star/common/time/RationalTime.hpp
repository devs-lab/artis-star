/**
 * @file common/time/RationalTime.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_TIME_RATIONAL_TIME
#define COMMON_TIME_RATIONAL_TIME

#include <artis-star/common/time/Time.hpp>

#include <boost/rational.hpp>

namespace artis::common {

struct RationalLimits {
  static constexpr boost::rational<int>
  negative_infinity = boost::rational<int>(std::numeric_limits<int>::lowest());
  static constexpr boost::rational<int>
  positive_infinity = boost::rational<int>(std::numeric_limits<int>::max());
  static constexpr boost::rational<int>
  null = boost::rational<int>();
};

typedef Time <boost::rational<int>, RationalLimits> RationalTime;

} // namespace artis common

#endif
