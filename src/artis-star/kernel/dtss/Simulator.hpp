/**
 * @file kernel/dtss/Simulator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DTSS_SIMULATOR
#define DTSS_SIMULATOR

#include <artis-star/common/Coordinator.hpp>
#include <artis-star/common/Parameters.hpp>
#include <artis-star/common/Simulator.hpp>
#include <artis-star/common/utils/Trace.hpp>

#include <cassert>

namespace artis::dtss {

template<typename Time, typename Dynamics, typename Parameters>
class Simulator;

template<typename Time, typename Dynamics, typename Parameters = common::NoParameters>
class Context {
  typedef dtss::Simulator<Time, Dynamics, Parameters> Simulator;

public:
  Context(const Parameters &parameters, Simulator *simulator)
    :
    _parameters(parameters), _simulator(simulator) {}

  virtual ~Context() {}

  const Parameters &parameters() const { return _parameters; }

  Simulator *simulator() const { return _simulator; }

private:
  const Parameters &_parameters;
  Simulator *_simulator;
};

template<typename Time, typename Dynamics, typename Parameters = common::NoParameters>
class Simulator : public common::Simulator<Time> {
  typedef Simulator<Time, Dynamics, Parameters> type;

public:
  Simulator(const std::string &name, const Parameters &parameters, const typename Time::type &time_step)
    :
    common::Model<Time>(name),
    common::Simulator<Time>(name),
    _dynamics(name, Context<Time, Dynamics, Parameters>(parameters, this)),
    _time_step(time_step) {}

  ~Simulator() {}

  virtual void restore(const common::context::State<Time> &state) {
    _dynamics.restore(state);
  }

  virtual void save(common::context::State<Time> &state) const {
    _dynamics.save(state);
  }

  virtual void finish(const typename Time::type &t) {
#ifndef WITH_TRACE
    (void) t;
#endif

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::DTSS,
                    common::FunctionType::FINISH,
                    common::LevelType::FORMALISM);
    common::Trace<Time>::trace().flush();
#endif
  }

  typename Time::type start(const typename Time::type &t) {

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                        common::FormalismType::DTSS,
                        common::FunctionType::I_MESSAGE,
                        common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    _dynamics.start(t);
    type::_tl = t;
    type::_tn = t;

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                        common::FormalismType::DTSS,
                        common::FunctionType::I_MESSAGE,
                        common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    return type::_tn;
  }

  typename Time::type lookahead(const typename Time::type &t) const {
    return _dynamics.lookahead(t);
  }

  common::event::Value observe(const typename Time::type &t,
                               unsigned int index) const { return _dynamics.observe(t, index); }

  virtual std::string observable_name(unsigned int observable_index) const {
    return _dynamics.observable_name(observable_index);
  }

  void output(const typename Time::type &t) {

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                        common::FormalismType::DTSS,
                        common::FunctionType::LAMBDA,
                        common::LevelType::FORMALISM)
                    << ": BEFORE";
    common::Trace<Time>::trace().flush();
#endif

    if (t == type::_tn) {
      common::event::Bag<Time> bag = _dynamics.lambda(t);

      if (not bag.empty()) {
        for (auto &event: bag) {
          event.set_model(this);
        }
        dynamic_cast < common::Coordinator<Time> * >(
          type::get_parent())->dispatch_events(bag, t);
      }
    }

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                        common::FormalismType::DTSS,
                        common::FunctionType::LAMBDA,
                        common::LevelType::FORMALISM)
                    << ": AFTER";
    common::Trace<Time>::trace().flush();
#endif

  }

  void post_event(const typename Time::type &t,
                  const common::event::ExternalEvent<Time> &event) {

#ifndef WITH_TRACE
    (void) t;
#endif

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                        common::FormalismType::DTSS,
                        common::FunctionType::POST_EVENT,
                        common::LevelType::FORMALISM)
            << ": BEFORE => " << event.to_string();
    common::Trace<Time>::trace().flush();
#endif

    type::add_event(event);

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                        common::FormalismType::DTSS,
                        common::FunctionType::POST_EVENT,
                        common::LevelType::FORMALISM)
            << ": AFTER => " << event.to_string();
    common::Trace<Time>::trace().flush();
#endif

  }

  typename Time::type transition(const typename Time::type &t) {

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                        common::FormalismType::DTSS,
                        common::FunctionType::S_MESSAGE,
                        common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    assert(t == type::_tn);

    _dynamics.transition(type::get_bag(), t);
    type::_tl = t;
    type::_tn = t + _time_step;
    type::clear_bag();

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                        common::FormalismType::DTSS,
                        common::FunctionType::S_MESSAGE,
                        common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    return type::_tn;
  }

  const typename Time::type &time_step() const { return _time_step; }

private :
  Dynamics _dynamics;
  typename Time::type _time_step;
};

} // namespace artis dtss

#endif
