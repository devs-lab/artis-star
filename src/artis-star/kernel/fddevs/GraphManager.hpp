/**
 * @file kernel/fddevs/GraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FDDEVS_GRAPH_MANAGER
#define FDDEVS_GRAPH_MANAGER

#include <artis-star/kernel/pdevs/GraphManager.hpp>

namespace artis::fddevs {

template<typename Time,
  typename Parameters = common::NoParameters,
  typename GraphParameters = common::NoParameters>
class GraphManager : public pdevs::GraphManager<Time, Parameters, GraphParameters> {
public:
  GraphManager(common::Coordinator <Time> *coordinator,
               const Parameters &parameters,
               const GraphParameters &graph_parameters)
    :
    pdevs::GraphManager<Time, Parameters, GraphParameters>(coordinator, parameters,
                                                           graph_parameters) {}

  ~GraphManager() override = default;
};

} // namespace artis fddevs

#endif
