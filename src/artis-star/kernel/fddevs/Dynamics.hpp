/**
 * @file kernel/fddevs/Dynamics.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FDDEVS_DYNAMICS
#define FDDEVS_DYNAMICS

#include "artis-star/common/event/Bag.hpp"
#include "artis-star/common/event/ExternalEvent.hpp"
#include <artis-star/common/Parameters.hpp>
#include "artis-star/common/state/States.hpp"
#include <artis-star/kernel/fddevs/Simulator.hpp>

#include <string>
#include <vector>

namespace artis::fddevs {

template<typename Time, typename Dyn, typename StateValues, typename Parameters = common::NoParameters>
class Dynamics : public common::state::States<Time, Dyn> {
  typedef fddevs::Simulator<Time, Dyn, Parameters> Simulator;

public:
  Dynamics(const std::string &name, const Context <Time, Dyn, Parameters> &context)
    :
    _name(name), _simulator(context.simulator()) {}

  virtual ~Dynamics() {}

  // definition
  void initial_state(const StateValues &state) { _initial_state = state; }

  // < X, Y, S, s0, tau, delta_x, sigma, delta_tau, lambda >
  virtual void delta_tau(typename Time::type /* t */) {}

  virtual void delta_x(typename Time::type /* t */, typename Time::type /* e */,
                       const common::event::Bag <Time> & /* bag */) {}

  virtual void initial_state(typename Time::type /* time */) { state(_initial_state); }

  virtual common::event::Bag <Time>
  lambda(typename Time::type /* time */) const { return common::event::Bag<Time>(); }

  virtual bool rho(typename Time::type /* time */,
                   const common::event::Bag <common::DoubleTime> & /* bag */) const { return false; }

  virtual typename Time::type
  tau(typename Time::type /* time */) const { return Time::infinity; }

  virtual typename Time::type lookahead(const typename Time::type &t) const { return t; }

  // observation
  virtual common::event::Value observe(const typename Time::type & /* t */,
                                unsigned int /* index */) const { return common::event::Value(); }

  // structure
  const std::string &get_name() const { return _name; }

  void input_port(common::Port p) {
    _simulator->add_in_port(p);
  }

  size_t input_port_number() const { return _simulator->get_in_port_number(); }

  void input_ports(std::initializer_list <common::Port> list) {
    for (typename std::initializer_list<common::Port>::iterator it = list.begin();
         it != list.end();
         ++it) {
      _simulator->add_in_port(*it);
    }
  }

  void output_port(common::Port p) {
    _simulator->add_out_port(p);
  }

  size_t output_port_number() const { return _simulator->get_out_port_number(); }

  void output_ports(std::initializer_list <common::Port> list) {
    for (typename std::initializer_list<common::Port>::iterator it =
      list.begin(); it != list.end(); ++it) {
      _simulator->add_out_port(*it);
    }
  }

  // state
  void restore(const common::context::State <Time> &state) {
    common::state::States<Time, Dyn>::restore(static_cast<Dyn *>(this), state);
  }

  void save(common::context::State <Time> &state) const {
    common::state::States<Time, Dyn>::save(static_cast<const Dyn *>(this), state);
  }

  void state(const StateValues &new_state) {
    _state = new_state;
  }

  const StateValues &state() const { return _state; }

private:
  std::string _name;
  Simulator *_simulator;

  // definition
  StateValues _initial_state;

  // state
  StateValues _state;
};

} // namespace artis fddevs

#endif
