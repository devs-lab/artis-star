/**
 * @file kernel/fddevs/Coordinator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FDDEVS_COORDINATOR
#define FDDEVS_COORDINATOR

#include <artis-star/kernel/pdevs/Coordinator.hpp>

namespace artis::fddevs {

template<typename Time,
  typename GraphManager,
  typename Parameters = common::NoParameters,
  typename GraphParameters = common::NoParameters>
class Coordinator
  : public pdevs::Coordinator<Time, GraphManager, Parameters, GraphParameters> {
public:
  Coordinator(const std::string &name, const Parameters &parameters,
              const GraphParameters &graph_parameters)
    :
    common::Model<Time>(name),
    pdevs::Coordinator<Time, GraphManager, Parameters, GraphParameters>(name,
                                                                        parameters,
                                                                        graph_parameters) {}

  virtual ~Coordinator() {}

  virtual std::string to_string(int level) const {
    std::ostringstream ss;

    ss << common::String::make_spaces(level * 2) << "fddevs coordinator \""
       << this->get_name() << "\":" << std::endl;
    ss << this->get_graph_manager().to_string(level + 1);
    return ss.str();
  }
};

} // namespace artis fddevs

#endif
