/**
 * @file kernel/devs/Dynamics.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEVS_DYNAMICS
#define DEVS_DYNAMICS

#include "artis-star/common/event/Bag.hpp"
#include "artis-star/common/event/ExternalEvent.hpp"
#include <artis-star/common/Parameters.hpp>
#include "artis-star/common/state/States.hpp"
#include <artis-star/kernel/devs/Simulator.hpp>

namespace artis::devs {

template<typename Time, typename Dyn, typename Parameters = common::NoParameters>
class Dynamics : public common::state::States<Time, Dyn> {
  typedef devs::Simulator<Time, Dyn, Parameters> Simulator;

public:
  struct Observable {
    unsigned int index;
    std::string name;
  };

  typedef std::map<unsigned int, std::string> Observables;

  Dynamics(const std::string &name, const Context<Time, Dyn, Parameters> &context)
    :
    _name(name), _simulator(context.simulator()) {}

  virtual ~Dynamics() {}

  virtual void dint(typename Time::type /* t */) {}

  virtual void dext(typename Time::type /* t */, typename Time::type /* e */,
                    const common::event::ExternalEvent<Time> & /* event */) {}

  virtual void start(typename Time::type /* time */) {}

  virtual typename Time::type
  ta(typename Time::type /* time */) const { return Time::infinity; }

  virtual common::event::ExternalEvent<Time>
  lambda(typename Time::type /* time */) const { return common::event::ExternalEvent<Time>(); }

  virtual common::event::Value observe(const typename Time::type & /* t */,
                                       unsigned int /* index */) const { return common::event::Value(); }

  virtual typename Time::type lookahead(const typename Time::type &t) const { return t; }

  const std::string &get_name() const { return _name; }

  void observable(Observable observable) {
    _observables[observable.index] = observable.name;
  }

  void observables(std::initializer_list<Observable> list) {
    for (typename std::initializer_list<Observable>::iterator it = list.begin();
         it != list.end();
         ++it) {
      _observables[it->index] = it->name;
    }
  }

  virtual std::string observable_name(unsigned int observable_index) const {
    assert(_observables.find(observable_index) != _observables.end());

    return _observables.find(observable_index)->second;
  }

  void restore(const common::context::State<Time> &state) {
    common::state::States<Time, Dyn>::restore(static_cast<Dyn *>(this), state);
  }

  void save(common::context::State<Time> &state) const {
    common::state::States<Time, Dyn>::save(static_cast<const Dyn *>(this), state);
  }

private:
  std::string _name;
  Simulator *_simulator;
  Observables _observables;
};

} // namespace artis devs

#endif
