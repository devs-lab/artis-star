/**
 * @file kernel/devs/GraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEVS_GRAPH_MANAGER
#define DEVS_GRAPH_MANAGER

#include <artis-star/common/Coordinator.hpp>
#include <artis-star/common/GraphManager.hpp>
#include <artis-star/common/Model.hpp>
#include <artis-star/common/Parameters.hpp>
#include <artis-star/common/utils/String.hpp>

#include <sstream>

namespace artis::devs {

template<typename Time,
  typename Parameters = common::NoParameters,
  typename GraphParameters = common::NoParameters>
class GraphManager : public common::GraphManager<Time> {
public:
  typedef GraphManager<Time, Parameters, GraphParameters> type;
  typedef std::pair<common::Model<Time> *, common::Model<Time> *> Link;
  typedef std::vector<Link> Links;

  GraphManager(common::Coordinator<Time> *coordinator,
               const Parameters & /* parameters */,
               const GraphParameters & /* graph_parameters */)
    :
    common::GraphManager<Time>(coordinator) {}

  ~GraphManager() override = default;

  void dispatch_events(common::event::Bag<Time> bag, typename Time::type t) {
    for (auto &ymsg: bag) {
      auto it = std::find_if(_links.begin(), _links.end(),
                             [&ymsg](const Link &link) { return link.first == ymsg.get_model(); });

      if (it != _links.end()) {
        // event on output port of coupled Model
        if (it->second == common::GraphManager<Time>::_coordinator) {
          dispatch_events_to_parent(ymsg.data(), t);
        } else { // event on input port of internal model
          it->second->post_event(t, common::event::ExternalEvent<Time>(ymsg.data()));
        }
      }
    }
  }

  virtual void
  dispatch_events_to_parent(const common::event::Value &content, typename Time::type t) {
    common::event::Bag<Time> ymessages;

    ymessages.push_back(common::event::ExternalEvent<Time>(content));
    if (common::GraphManager<Time>::_coordinator->get_parent()) {
      dynamic_cast < common::Coordinator<Time> * >(common::GraphManager<Time>::_coordinator
        ->get_parent())
        ->dispatch_events(ymessages, t);
    }
  }

  bool exist_link(common::Model<Time> *src_model, common::Model<Time> *dst_model) const {
    return _links.find(std::make_pair(src_model, dst_model));
  }

  virtual typename Time::type lookahead(const typename Time::type &t) const { return t; }

  void post_event(typename Time::type t, const common::event::ExternalEvent<Time> &event) {
    auto it = std::find_if(_links.begin(), _links.end(),
                           [this](const Link &link) {
                             return link.first == common::GraphManager<Time>::_coordinator;
                           });

    if (it != _links.end()) {
      it->second->post_event(t,
                             common::event::ExternalEvent<Time>(event.data()));
    }
  }

  virtual std::string to_string(int level) const {
    std::ostringstream ss;

    ss << common::String::make_spaces(level * 2) << "Children :" << std::endl;
    for (auto &child: common::GraphManager<Time>::_children) {
      ss << child->to_string(level + 1);
    }
    ss << common::String::make_spaces(level * 2) << "Links:" << std::endl;
    for (typename Links::const_iterator it = _links.begin(); it != _links.end(); ++it) {
      ss << common::String::make_spaces((level + 1) * 2)
         << it->first->get_name() << " -> "
         << it->second->get_name() << std::endl;
    }
    return ss.str();
  }

  void add_link(common::Model<Time> *src_model, common::Model<Time> *dst_model) {
    assert((src_model != common::GraphManager<Time>::_coordinator and
            dst_model != common::GraphManager<Time>::_coordinator and
            src_model != dst_model) or
           (src_model == common::GraphManager<Time>::_coordinator and
            dst_model != common::GraphManager<Time>::_coordinator) or
           (src_model != common::GraphManager<Time>::_coordinator and
            dst_model == common::GraphManager<Time>::_coordinator));

    _links.push_back(std::make_pair(src_model, dst_model));
  }

private:
  Links _links;
};

} // namespace artis devs

#endif
