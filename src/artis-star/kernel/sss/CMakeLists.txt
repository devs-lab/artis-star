INCLUDE_DIRECTORIES(
        ${ARTIS_STAR_BINARY_DIR}/src
        ${ARTIS_STAR_SOURCE_DIR}/src
        ${Boost_INCLUDE_DIRS})

LINK_DIRECTORIES(
        ${Boost_LIBRARY_DIRS})

SET(SSS_HPP Context.hpp Coordinator.hpp Dynamics.hpp GraphManager.hpp Model.hpp Simulator.hpp)

ADD_SOURCES(artislib ${SSS_HPP})

INSTALL(FILES ${SSS_HPP} DESTINATION ${ARTIS_STAR_INCLUDE_DIRS}/kernel/sss)