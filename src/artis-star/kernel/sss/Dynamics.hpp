/**
 * @file kernel/sss/Dynamics.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SSS_DYNAMICS
#define SSS_DYNAMICS

#include <artis-star/kernel/sss/Context.hpp>
#include <artis-star/kernel/sss/Simulator.hpp>
#include "artis-star/common/state/States.hpp"

namespace artis::sss {

template<typename Time, typename Dyn, typename Parameters = common::NoParameters>
class Dynamics : public common::state::States<Time, Dyn> {
  typedef sss::Simulator<Time, Dyn, Parameters> Simulator;

public:
  Dynamics(const std::string &name, const Context<Time, Dyn, Parameters> &context)
    :
    _name(name), _simulator(context.simulator()) {}

  virtual ~Dynamics() = default;

  virtual void transition(const common::event::Bag<Time> & /* x */,
                          typename Time::type /* t */) {}

  virtual typename Time::type start(typename Time::type/* time */) { return Time::infinity; }

  common::event::Bag<Time> lambda(typename Time::type /* time */) const { return common::event::Bag<Time>(); }

  virtual void observation(std::ostream & /* file */) const {}

  const std::string &get_name() const { return _name; }

  void restore(const common::context::State<Time> &state) {
    common::state::States<Time, Dyn>::restore(static_cast<Dyn *>(this), state);
    _simulator->set_tn(state.last_time());
    _simulator->set_tl(state.next_time());
  }

  void save(common::context::State<Time> &state) const {
    common::state::States<Time, Dyn>::save(static_cast<const Dyn *>(this), state);
    state.last_time(_simulator->get_tn());
    state.next_time(_simulator->get_tl());
  }

  virtual void update_buffer(typename Time::type /* time */) {}

private:
  std::string _name;
  Simulator *_simulator;
};

} // namespace artis sss

#endif
