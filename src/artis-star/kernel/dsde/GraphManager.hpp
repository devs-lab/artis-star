/**
 * @file kernel/dsde/GraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DSDE_GRAPH_MANAGER
#define DSDE_GRAPH_MANAGER

#include <artis-star/kernel/pdevs/GraphManager.hpp>

#include <vector>

namespace artis::dsde {

template<typename Time,
  typename Parameters = common::NoParameters,
  typename GraphParameters = common::NoParameters>
class GraphManager : public pdevs::GraphManager<Time, Parameters, GraphParameters> {
public:
  typedef pdevs::GraphManager <Time, Parameters, GraphParameters> super;
  typedef GraphManager<Time, Parameters, GraphParameters> type;

  GraphManager(common::Coordinator <Time> *coordinator,
               const Parameters &parameters,
               const GraphParameters &graph_parameters)
    :
    pdevs::GraphManager<Time, Parameters, GraphParameters>(coordinator, parameters,
                                                           graph_parameters) {}

  ~GraphManager() override {
    for (auto m: _models) {
      delete m;
    }
  }

  virtual void add_dynamic_child(unsigned int index, common::Model <Time> *child) {
    super::add_child(index, child);
    _models.push_back(child);
    _new_models.push_back(child);
  }

  virtual void add_dynamic_children(unsigned int index, common::Model <Time> *child) {
    super::add_children(index, child);
    _models.push_back(child);
    _new_models.push_back(child);
  }

  void add_link(unsigned int source_model_index, unsigned int source_port_index,
                unsigned int destination_model_index, unsigned int destination_port_index) {
    common::Model <Time> *source_model = super::child_map(source_model_index);
    common::Model <Time> *destination_model = super::child_map(destination_model_index);

    assert(source_model and destination_model);

    this->out({source_model, source_port_index})
      >> this->in({destination_model, destination_port_index});
  }

  void clear_new_models() { _new_models.clear(); }

  std::vector<common::Model < Time> *> &

  new_models() { return _new_models; }

  void remove_dynamic_child(const typename Time::type &t, unsigned int index) {
    size_t new_index = index
                       - (pdevs::GraphManager<Time, Parameters, GraphParameters>::_children.size()
                          - _models.size());

    new_index -= std::count_if(_models.begin(), _models.end(),
                               [](common::Model <Time> *model) { return model == nullptr; });
    type::coordinator()->remove_model(t, _models[new_index]);
    super::remove_child(index);
    this->remove_links(_models[new_index]);
    delete _models[new_index];
    _models[new_index] = nullptr;
  }

  void remove_link(unsigned int source_model_index, unsigned int source_port_index,
                   unsigned int destination_model_index, unsigned int destination_port_index) {
    common::Model <Time> *source_model = super::child_map(source_model_index);
    common::Model <Time> *destination_model = super::child_map(destination_model_index);

    assert(source_model and destination_model);

    super::remove_link(source_model, source_port_index, destination_model,
                       destination_port_index);
  }

private:
  std::vector<common::Model < Time> *>
  _models;
  std::vector<common::Model < Time> *>
  _new_models;
};

}

#endif