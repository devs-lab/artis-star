/**
 * @file kernel/pdevs/Coordinator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PDEVS_COORDINATOR
#define PDEVS_COORDINATOR

#include <artis-star/common/Coordinator.hpp>
#include <artis-star/common/Parameters.hpp>
#include <artis-star/common/Scheduler.hpp>
#include <artis-star/common/utils/String.hpp>
#include <artis-star/common/utils/Trace.hpp>

#include <cassert>

namespace artis::pdevs {

template<typename Time,
  typename GraphManager,
  typename Parameters = common::NoParameters,
  typename GraphParameters = common::NoParameters>
class Coordinator : public common::Coordinator<Time> {
  typedef Coordinator<Time, GraphManager, Parameters, GraphParameters> type;

public:
  typedef Parameters parameters_type;
  typedef GraphParameters graph_parameters_type;

  Coordinator(const std::string &name, const Parameters &parameters,
              const GraphParameters &graph_parameters)
    :
    common::Model<Time>(name),
    common::Coordinator<Time>(name),
    _graph_manager(this, parameters, graph_parameters) {}

  virtual ~Coordinator() {}

  GraphManager &get_graph_manager() override { return _graph_manager; }

  const GraphManager &get_graph_manager() const override { return _graph_manager; }

  std::string to_string(int level) const override {
    std::ostringstream ss;

    ss << common::String::make_spaces(level * 2) << "p-devs coordinator \""
       << type::get_name() << "\":" << std::endl;
    ss << _graph_manager.to_string(level + 1);
    return ss.str();
  }

  void restore(const common::context::State<Time> &state) override {
    common::Coordinator<Time>::restore(state);
    for (auto &child: _graph_manager.children()) {
      _event_table.init(child->get_tn(), child);
    }
  }

  virtual void finish(const typename Time::type &t) override {
#ifndef WITH_TRACE
    (void) t;
#endif

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::FINISH,
                    common::LevelType::FORMALISM);
    common::Trace<Time>::trace().flush();
#endif
  }

  typename Time::type start(const typename Time::type &t) override {
//                When i-message (i, t) at time t
//                  for-each d ∈ D do
//                    send i-message to child d
//                  sort event-list according to tn,d
//                  tl ← t
//                  tn ← min{tn,d | d ∈ D}
//                End

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::I_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    assert(_graph_manager.children().size() > 0);

    for (auto &child: _graph_manager.children()) {
      _event_table.init(child->start(t), child);
    }
    type::_tl = t;
    type::_tn = _event_table.get_current_time();

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::I_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl
            << " ; tn = " << type::_tn;
    common::Trace<Time>::trace().flush();
#endif

    return type::_tn;
  }

  void output(const typename Time::type &t) override {
//                When *-message (*, t)
//                  if t != tn then Error
//                  IMM = {d | (d, th,d) ∈ (event-list & tn,d = tn) }
//                  for-each r ∈ IMM
//                    send *-message (*, t) to r
//                End

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::OUTPUT,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn << " ; scheduler = " << _event_table.to_string();
    common::Trace<Time>::trace().flush();
#endif

    assert(t == type::_tn);

    common::Models<Time> IMM = _event_table.get_current_models(t);

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::OUTPUT,
                    common::LevelType::FORMALISM)
            << ": IMM = " << IMM.to_string();
    common::Trace<Time>::trace().flush();
#endif

    for (auto &model: IMM) {
      model->output(t);
    }

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::OUTPUT,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn << " ; scheduler = " << _event_table.to_string();
    common::Trace<Time>::trace().flush();
#endif

  }

  typename Time::type transition(const typename Time::type &t) override {
//                When x-message (x, t)
//                  if not (tl <= t <= tn) then Error
//                  receivers = { r | r ∈ children, N ∈ Ir, Z(N,r)(x) isn't empty }
//                  for each r ∈ receivers
//                    send x-message(Z(N,r)(x), t) with input value Z(N,r)(x) to r
//                  for each r ∈ IMM and not in receivers
//                    send x-message(empty, t) to r
//                  sort event list according to tn
//                  tl = t
//                  tn = min(tn,d | d ∈ D)
//                End

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::S_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn << " ; scheduler = " << _event_table.to_string();
    common::Trace<Time>::trace().flush();
#endif

    assert(t >= type::_tl and t <= type::_tn);

    common::Models<Time> receivers = get_receivers();
    common::Models<Time> IMM = _event_table.get_current_models(t);

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::S_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": receivers = " << receivers.to_string()
            << " ; IMM = " << IMM.to_string();
    common::Trace<Time>::trace().flush();
#endif

    for (auto &model: receivers) {
      _event_table.put(model->transition(t), model);
    }
    for (auto &model: IMM) {
      if (std::find(receivers.begin(), receivers.end(), model) == receivers.end()) {
        _event_table.put(model->transition(t), model);
      }
    }
//                update_event_table(t);
    type::_tl = t;
    type::_tn = _event_table.get_current_time();
    type::clear_bag();

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::S_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn << " ; scheduler = " << _event_table.to_string();
    common::Trace<Time>::trace().flush();
#endif

    return type::_tn;
  }

  void post_event(const typename Time::type &t, const common::event::ExternalEvent<Time> &event) override {

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::POST_EVENT,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << event.to_string();
    common::Trace<Time>::trace().flush();
#endif

    type::add_event(event);
    _graph_manager.post_event(t, event);
//                update_event_table(t);
    type::_tn = _event_table.get_current_time();

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::POST_EVENT,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << event.to_string();
    common::Trace<Time>::trace().flush();
#endif

  }

  typename Time::type dispatch_events(const common::event::Bag<Time> &bag, const typename Time::type &t) override {

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::Y_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": BEFORE => " << "tl = " << type::_tl << " ; tn = "
            << type::_tn << " ; bag = " << bag.to_string()
            << " ; " << _event_table.to_string();
    common::Trace<Time>::trace().flush();
#endif

    _graph_manager.dispatch_events(bag, t);
//                update_event_table(t);
    type::_tn = _event_table.get_current_time();

#ifdef WITH_TRACE
    common::Trace<Time>::trace()
            << common::TraceElement<Time>(type::get_name(), t,
                    common::FormalismType::PDEVS,
                    common::FunctionType::Y_MESSAGE,
                    common::LevelType::FORMALISM)
            << ": AFTER => " << "tl = " << type::_tl << " ; tn = " << type::_tn
            << " ; " << _event_table.to_string();
    common::Trace<Time>::trace().flush();
#endif

    return type::_tn;
  }

  common::event::Value observe(const typename Time::type & /* t */, unsigned int /* index */) const override {
    assert(false);
    return common::event::Value();
  }

  typename Time::type lookahead(const typename Time::type &t) const override {
    return _graph_manager.lookahead(t);
  }

  common::Models<Time> get_receivers() const {
    common::Models<Time> receivers;

    for (auto &model: _graph_manager.children()) {
      if (model->event_number() > 0) {
        receivers.push_back(model);
      }
    }
    return receivers;
  }

  void update_event_table(typename Time::type t) {
    for (auto &model: _graph_manager.children()) {
      if (model->event_number() > 0) {
        _event_table.put(t, model);
      }
    }
  }

protected:
  GraphManager _graph_manager;
  common::SchedulerType _event_table;
};

} // namespace artis pdevs

#endif
