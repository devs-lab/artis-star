/**
 * @file kernel/pdevs/Context.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PDEVS_CONTEXT
#define PDEVS_CONTEXT

namespace artis::pdevs {

template<typename Time, typename Dynamics, typename Parameters>
class Simulator;

template<typename Time, typename Dynamics, typename Parameters = common::NoParameters>
class Context {
  typedef pdevs::Simulator<Time, Dynamics, Parameters> Simulator;

public:
  Context(const Parameters &parameters, Simulator *simulator)
    :
    _parameters(parameters), _simulator(simulator) {}

  virtual ~Context() {}

  const Parameters &parameters() const { return _parameters; }

  Simulator *simulator() const { return _simulator; }

private:
  const Parameters &_parameters;
  Simulator *_simulator;
};

} // namespace artis pdevs

#endif
